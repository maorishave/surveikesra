<?php

namespace App\Http\Middleware;

use Closure;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        foreach ($role as $roles) {
            if ($request->session()->get('activeUser')->role == $roles) {
                return $next($request);
            }
        }
        if ($request->session()->get('activeUser')->role != $role){
            return abort(503, 'Anda tidak memiliki hak akses');
        }
    }
}
