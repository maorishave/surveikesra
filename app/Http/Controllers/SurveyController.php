<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SurveyController extends Controller
{
    public function indexModerator(){
        return view('backend.survey.index_moderator');
    }

    public function indexSurveyor(){
        return view('backend.survey.index');
    }

    public function indexCreate(){
        return view('backend.survey.index_create');
    }

    public function getSurveymoderator(){
        $survey = Survey::all();
        return DataTables::of($survey)
            ->addColumn('action', function ($survey) {
                if ($survey->status==1){
                    return '<a onclick="loadModal(this)" target="" data="" class="btn btn-info btn-xs" title="Verifikasi" style="color: #ffffff">
<i class="fas fa-checked"></i>
                Verifikasi</a>';
                }
                return '<a onclick="loadModal(this)" target="" data="" class="btn btn-info btn-xs" title="Verifikasi" style="color: #ffffff">
<i class="fas fa-minus"></i></a>';
            })
            ->make(true);
    }

    public function getSurveyor(){
        $survey = Survey::all();
        return DataTables::of($survey)
            ->make(true);
    }

    public function createSurvey(Request $request){
        $data=new Survey();
        $data->nama_orang=$request->data_nama;
        $data->NIK=$request->data_nik;
        $data->tgl_lahir=$request->ttl;
        $data->jns_kelamin=$request->gender;
        $data->jns_pendidikan=$request->pendidikan;
        $data->status=0;
        try{
            $data->save();
            return redirect('survey-surveyor');
        }catch (\Exception $e){
        }
    }

    public function verifSurvey(Request $request){
        $id=$request->id;
        $data=Survey::find($id);
        $data->status=1;
        try{
            $data->save();
            return redirect('survey-moderator');
        }catch (\Exception $e){
        }
    }
}
