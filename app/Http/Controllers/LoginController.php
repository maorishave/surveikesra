<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request){
        if ($request->session()->exists('activeUser')){
            return redirect('/backend');
        }
        return view('login.index');
    }

    public function login(Request $request){
        $username=$request->username;
        $password=$request->password;

        $activeUser=Users::where(['username'=>$username])->first();
        if (is_null($activeUser)){
            return redirect('/login')->with('error', 'Pengguna tidak terdaftar');
        }else{
            if ($activeUser->password==sha1($password)){
                $request->session()->put('activeUser', $activeUser);
                return redirect('/');
            }else{
                return redirect('/login')->with('error', 'Password salah');
            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
