@extends('layout.main')
@section('content-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Survey</span> - Masyarakat</h4>

    <ul class="breadcrumb breadcrumb-caret position-right">
        <li><a href="index.html">Home</a></li>
        <li class="active">Data Survey</li>
    </ul>
@endsection
@section('content')
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page length options -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Page length options</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <table class="table datatable-show-all">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Tanggal Lahir</th>
                            <th>Jenis Kelamin</th>
                            <th>Jenis Pendidikan</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /page length options -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/datatables.net-select-bs4/js/select.bootstrap4.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var tableData = $('#table-data').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{url('dataModerator')}}',
                columns: [
                    {data: 'id_survey',},
                    {data: 'nama_orang',},
                    {data: 'NIK',},
                    {data: 'tgl_lahir',},
                    {data: 'jns_kelamin',},
                    {data: 'jns_pendidikan',},
                    {data: 'action', orderable: false, searchable: false}
                ],
                pageLength: 10,
            });
        });
    </script>
@endsection
