@extends('layout.main')
@section('content-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Survey</span> - Masyarakat</h4>

    <ul class="breadcrumb breadcrumb-caret position-right">
        <li><a href="index.html">Home</a></li>
        <li class="active">Tambah Survey</li>
    </ul>
@endsection
@section('content')
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form horizontal -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Tambah Data Survey</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" action="{{url('simpanData')}}" method="POST">
                            {{csrf_field()}}
                            <fieldset class="content-group">
                                <legend class="text-bold"></legend>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nama Orang</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="data_nama">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">NIK</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="data_nik">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Tgl Lahir</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="date" name="ttl">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Jenis Kelamin</label>
                                    <div class="col-md-10">
                                        <input type="radio" name="gender" value="laki">Laki
                                        <input type="radio" name="gender" value="perempuan">Perempuan
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Riwayat Pendidikan</label>
                                    <div class="col-md-10">
                                        <select class="bootstrap-select" data-width="100%" name="pendidikan">
                                            <option value="sd">SD</option>
                                            <option value="smp">SMP</option>
                                            <option value="sma">SMA</option>
                                            <option value="diploma">Diploma</option>
                                            <option value="sarjana">Sarjana</option>
                                        </select>
                                    </div>
                                </div>

                            </fieldset>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /form horizontal -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/datatables.net-select-bs4/js/select.bootstrap4.min.js')}}"></script>
    <script>

    </script>
@endsection
