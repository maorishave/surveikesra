<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//role 1:moderator
//role 2:surveyor


Route::get('login', 'LoginController@index');

Route::post('validasi-login','LoginController@login');
Route::get('logout', 'LoginController@logout');

Route::group(['middleware' => 'check_role:1,2'], function () {
    Route::get('/', 'LoginController@index');
    Route::get('backend', 'BackendController@index');
});

Route::group(['middleware' => 'check_role:1'], function () {
    Route::get('survey-moderator', 'SurveyController@indexModerator');
    Route::get('dataModerator', 'SurveyController@getSurveymoderator');
});

Route::group(['middleware' => 'check_role:2'], function () {
    Route::get('survey-surveyor', 'SurveyController@indexSurveyor');
    Route::get('dataSurveyor', 'SurveyController@getSurveyor');
    Route::get('indexAdd','SurveyController@indexCreate');
    Route::post('simpanData','SurveyController@createSurvey');
});
